// eslint-disable-next-line @typescript-eslint/no-var-requires
const colors = require('tailwindcss/colors');

const primary = {
  DEFAULT: '#251F47',
  50: '#928fa3',
  100: '#7c7991',
  200: '#66627e',
  300: '#514c6c',
  400: '#3b3559',
  500: '#251F47',
  600: '#211c40',
  700: '#1e1939',
  800: '#1a1632',
  900: '#16132b',
};

module.exports = {
  theme: {
    extend: {
      colors: {
        black: '#333333',
        primary: primary,
        info: colors.sky,
        success: colors.emerald,
        warning: colors.yellow,
        error: colors.rose,
      },
      fontFamily: {
        // display: ['Poppins', 'Arial', 'sans-serif'],
        sans: [
          'Inter',
          'ui-sans-serif',
          'system-ui',
          '-apple-system',
          'BlinkMacSystemFont',
          '"Segoe UI"',
          'Roboto',
          '"Helvetica Neue"',
          'Arial',
          '"Noto Sans"',
          'sans-serif',
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"',
          '"Noto Color Emoji"',
        ],
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
  ],
};

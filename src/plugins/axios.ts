import axios from 'axios'

axios.defaults.baseURL = String(import.meta.env.VITE_API_BASE_URL)
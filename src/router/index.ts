import { createRouter, createWebHistory } from 'vue-router'
import ListStarships from '../views/index.vue'
import DetailStarships from '../views/starships/detail.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'List Starships',
      component: ListStarships
    },
    {
      path: '/starships/:id',
      name: 'Detail Starships',
      component: DetailStarships
    },
  ]
})

export default router

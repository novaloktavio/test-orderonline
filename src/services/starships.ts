import axios from 'axios'

export class StarshipsService {
  async getList(params: any) {
    return axios.get('/starships', { params })
  }

  async getDetail(id: any) {
    return axios.get(`/starships/${id}`)
  }
}

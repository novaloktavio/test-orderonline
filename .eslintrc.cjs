/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  extends: '@antfu',
  rules: {
    'vue/valid-v-slot': ['error', {
      allowModifiers: true,
    }],
    'vue/custom-event-name-casing': ['error',
      'camelCase',
      {
        ignores: ['/^[a-z]+(?:-[a-z]+)*:[a-z]+(?:-[a-z]+)*$/u'],
      },
    ],
    'vue/define-macros-order': 'off'
  },
}
